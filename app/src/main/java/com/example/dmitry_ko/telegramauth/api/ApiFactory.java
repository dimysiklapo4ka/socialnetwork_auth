package com.example.dmitry_ko.telegramauth.api;

public class ApiFactory {

    private static ApiFactory INSTANCE;
    private ApiRepository mApiRepository;

    private ApiFactory(){
        mApiRepository = ApiRepository.getInstance();
    }

    public static ApiFactory getINSTANCE() {
        if (INSTANCE == null){
            INSTANCE = new ApiFactory();
        }
        return INSTANCE;
    }

    public ApiRepository getApiRepository(){
        return mApiRepository;
    }
}
