package com.example.dmitry_ko.telegramauth.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.dmitry_ko.telegramauth.R;
import com.example.dmitry_ko.telegramauth.databinding.ActivitySecondBinding;
import com.example.dmitry_ko.telegramauth.viewmodel.SecondViewModel;

public class SecondActivity extends AppCompatActivity {

    private SecondViewModel mViewModel;
    private ActivitySecondBinding mDataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
        setText();
    }

    private void initViewModel() {
        mDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_second);
        mViewModel = new SecondViewModel();
        mDataBinding.setMSecondViewModel(mViewModel);
    }

    private void setText(){
        if (getIntent().getAction() != null){
            String text = getIntent().getStringExtra("CODE_SMS_VERIFIED");
            Log.e(SecondActivity.class.getSimpleName(), text);
            mViewModel.setText(text);
        }
    }
}
