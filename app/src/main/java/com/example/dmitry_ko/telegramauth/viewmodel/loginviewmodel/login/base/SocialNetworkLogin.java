package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base;

import android.content.Intent;

public interface SocialNetworkLogin {
    void login();
    void activityResult(int requestCode, int resultCode, Intent data);
}
