package com.example.dmitry_ko.telegramauth.api;

import com.example.dmitry_ko.telegramauth.model.TelegramApi;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApiRepository{

    private static ApiRepository INSTANCE;
    private TelegramApi mApi;

    private int app_api_id = 546349;
    private String app_api_hash ="d558cce33c4ba4fefbdf35d91e623ad5";

    private ApiRepository(){
        mApi = RetrofitClient.getINSTANCE().getTelegramApi();
    }

    public static ApiRepository getInstance(){
        if (INSTANCE == null){
            INSTANCE = new ApiRepository();
        }
        return INSTANCE;
    }

    public Single<Object> getRequest(String phoneNumber){
        return mApi.sendSmsAPhoneNumber(phoneNumber,
                0,
                app_api_id,
                app_api_hash,
                "ua")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
