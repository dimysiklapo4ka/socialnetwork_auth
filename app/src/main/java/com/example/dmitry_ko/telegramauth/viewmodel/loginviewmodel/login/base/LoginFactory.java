package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base;

import android.content.Context;

import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.LoginWithFacebook;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.LoginWithGoogle;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.LoginWithTelegram;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.FacebookListener;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.GoogleListener;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.TelegramListener;

public class LoginFactory {
    private static LoginFactory INSTANCE;

    private LoginFactory(){}

    public static LoginFactory getInstance(){
        if (INSTANCE == null){
            INSTANCE = new LoginFactory();
        }
        return INSTANCE;
    }

    public SocialNetworkLogin getConcretLogin(String loginWith, Context context, SocialNetworkListener socialNetworkListener) {
        SocialNetworkLogin mLogin = null;
        switch (loginWith) {
            case "LoginWithGoogle":
                mLogin = LoginWithGoogle.getInstance(context, (GoogleListener)socialNetworkListener);
                return mLogin;
            case "LoginWithFacebook":
                mLogin = LoginWithFacebook.getInstance(context, (FacebookListener)socialNetworkListener);
                return mLogin;
            case "LoginWithTelegram":
                mLogin = LoginWithTelegram.getInstance(context, (TelegramListener)socialNetworkListener);
                return mLogin;
            default:
                return mLogin;
        }
    }
}
