package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login;

import android.content.Context;
import android.content.Intent;

import com.example.dmitry_ko.telegramauth.activity.LoginActivity;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.SocialNetworkLogin;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.FacebookListener;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

public class LoginWithFacebook implements SocialNetworkLogin {
    private static LoginWithFacebook INSTANCE;
    private Context context;
    private CallbackManager callbackManager;

    private FacebookListener mListener;

    private LoginWithFacebook(Context context, FacebookListener facebookListener){
        this.context = context;
        mListener = facebookListener;
        initFacebookAuth();
    }

    public static LoginWithFacebook getInstance(Context context, FacebookListener facebookListener){
        if (INSTANCE == null){
            INSTANCE = new LoginWithFacebook(context, facebookListener);
        }
        return INSTANCE;
    }

    private void initFacebookAuth(){
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void login() {
        LoginManager.getInstance().logInWithReadPermissions(((LoginActivity)context), Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mListener.facebookResult(loginResult);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    @Override
    public void activityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
