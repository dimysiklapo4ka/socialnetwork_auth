package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.os.Bundle;
import android.view.View;

import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.LoginWithFacebook;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.LoginWithGoogle;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.LoginWithTelegram;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.LoginFactory;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.FacebookListener;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.GoogleListener;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.TelegramListener;
import com.example.dmitry_ko.telegramauth.api.ApiFactory;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.telegram.passport.TelegramPassport;

import io.reactivex.disposables.CompositeDisposable;

import static android.app.Activity.RESULT_OK;

public class MainViewModel extends BaseObservable implements FacebookListener, GoogleListener, TelegramListener {

    public static final int GOOGLE_SIGN_IN = 1024;
    public static final int TELEGRAM_SIGN_IN = 1027;

    private Context context;
    private LoginWithGoogle mLoginWithGoogle;
    private LoginWithFacebook mLoginWithFacebook;
    private LoginWithTelegram mLoginWithTelegram;

    //rxAndroid -> close Single
    private CompositeDisposable mDisposableContainer = new CompositeDisposable();

    public MainViewModel(Context context){
        this.context = context;
    }

    public void googleClicked(View view){
        mLoginWithGoogle = (LoginWithGoogle) LoginFactory.getInstance().getConcretLogin(LoginWithGoogle.class.getSimpleName(), context, this);
        mLoginWithGoogle.login();
    }

    public void facebookClicked(View view){
        mLoginWithFacebook = (LoginWithFacebook) LoginFactory.getInstance().getConcretLogin(LoginWithFacebook.class.getSimpleName(),context, this);
        mLoginWithFacebook.login();
    }

    public void telegramClicked(View view){
        mLoginWithTelegram = (LoginWithTelegram) LoginFactory.getInstance().getConcretLogin(LoginWithTelegram.class.getSimpleName(),context, this);
        mLoginWithTelegram.login();
    }

    public void emailClicked(View view){
        mDisposableContainer.add(ApiFactory.getINSTANCE().getApiRepository().getRequest("+380502276647")
        .subscribe(o -> displayAlert("email", "EMAIL VERIFIED"),
                throwable ->
                    displayAlert("Email", "JSON not parsed")
                ));
    }

    public void handleGoogleSignInResult(Task<GoogleSignInAccount> task) {
        if (task.isSuccessful()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = task.getResult();
            if (acct != null) {
                displayAlert("Google Auth", String.format("%s\nEmail : %s", acct.getDisplayName(), acct.getEmail()));
            }
        }
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            String name = null;
            try {
                name = object.getString("name");
                String email = object.getString("email");

                displayAlert(name, email);
            } catch (JSONException e) {
                if (name != null) {
                    displayAlert(name, "");
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void displayAlert(String title, String message){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                .show();
    }

    public void unsubscribe(){
        mDisposableContainer.clear();
    }

    public void resultActivity(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainViewModel.GOOGLE_SIGN_IN) {
            mLoginWithGoogle.activityResult(requestCode, resultCode, data);
        }else if(requestCode==TELEGRAM_SIGN_IN){
            mLoginWithTelegram.activityResult(requestCode, resultCode, data);
        }else{
            mLoginWithFacebook.activityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void facebookResult(LoginResult loginResult) {
        handleFacebookAccessToken(loginResult.getAccessToken());
    }

    @Override
    public void googleResult(Task<GoogleSignInAccount> googleSignInAccount) {
        handleGoogleSignInResult(googleSignInAccount);
    }

    @Override
    public void telegramResult(int requestCode, int resultCode, Intent data) {
        if(resultCode==RESULT_OK){
            displayAlert("Можно Логинить", "Типа Залогинились");
            }else if(resultCode==TelegramPassport.RESULT_ERROR){
            displayAlert("Ошибка авторизации Telegram", "error");
            }else{
            displayAlert("Авторизация отменена", "Охрана ОТМЕНА");
            }
    }
}