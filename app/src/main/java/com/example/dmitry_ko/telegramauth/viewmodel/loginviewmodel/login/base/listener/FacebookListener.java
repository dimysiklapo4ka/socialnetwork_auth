package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener;

import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.SocialNetworkListener;
import com.facebook.login.LoginResult;

public interface FacebookListener extends SocialNetworkListener {
    void facebookResult(LoginResult loginResult);
}
