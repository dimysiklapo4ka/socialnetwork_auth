package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener;

import android.content.Intent;

import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.SocialNetworkListener;

public interface TelegramListener extends SocialNetworkListener {
    void telegramResult(int requestCode, int resultCode, Intent data);
}
