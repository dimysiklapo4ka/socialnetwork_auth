package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener;

import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.SocialNetworkListener;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.Task;

public interface GoogleListener extends SocialNetworkListener {
    void googleResult(Task<GoogleSignInAccount> googleSignInAccount);
}
