package com.example.dmitry_ko.telegramauth.data;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtils {

    private static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String PHONE_NUMBER_IS_VERIFIED = "PHONE_NUMBER_IS_VERIFIED";

    private static SharedPrefUtils INSTANCE;
    private static Context context;

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static final String NameSharedPrefData = "AUTH_TELEGRAM";

    private SharedPrefUtils(){
    }

    /**
    * Singleton class for saved data
    */

    public static SharedPrefUtils getINSTANCE(){
        if (INSTANCE == null){
            INSTANCE = new SharedPrefUtils();
        }
        return INSTANCE;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private static void init(){
        if (sharedPreferences == null){
            sharedPreferences = context.getSharedPreferences(NameSharedPrefData, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
    }

    /**
     * public method for getting and saving data
     */


    /**
     * method from saving phone number
     */
    public static void setPhoneNumber(String phoneNumber){
        setString(PHONE_NUMBER, phoneNumber);
    }

    /**
     * method from getting phone number
     */
    public static String getPhoneNumber(){
        return getString(PHONE_NUMBER);
    }

    /**
     * method from saving verified phone number
     */
    public static void setVerifiedPhoneNumber(boolean isVerified){
        setBoolean(PHONE_NUMBER_IS_VERIFIED, isVerified);
    }

    /**
     * method from geting is verified phone number
     */
    public static Boolean getVerifiedPhoneNumber(){
        return getBoolean(PHONE_NUMBER_IS_VERIFIED);
    }

    /**
     * Static methods from entering and reading data from shared preferences
     *
     * default data : ->
     *
     *      boolean = false
     *      string = ""
     *
     *      */

    private static final boolean DEFAULT_BOOLEAN = false;
    private static final String DEFAULT_STRING = "";

    private static void setBoolean(String name, boolean result){
        if (sharedPreferences == null){
            init();
        }
        editor.putBoolean(name, result);
        editor.apply();
    }

    private static Boolean getBoolean(String name){
        if (sharedPreferences == null){
            init();
        }
        return sharedPreferences.getBoolean(name, DEFAULT_BOOLEAN);
    }

    private static void setString(String name, String result){
        if (sharedPreferences == null){
            init();
        }
        editor.putString(name, result);
    }

    private static String getString(String name){
        if (sharedPreferences == null){
            init();
        }
        return sharedPreferences.getString(name, DEFAULT_STRING);
    }
}
