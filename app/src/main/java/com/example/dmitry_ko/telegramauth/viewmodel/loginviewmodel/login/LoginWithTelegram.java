package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login;

import android.content.Context;
import android.content.Intent;

import com.example.dmitry_ko.telegramauth.activity.LoginActivity;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.SocialNetworkLogin;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.TelegramListener;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.MainViewModel;

import org.telegram.passport.PassportScope;
import org.telegram.passport.TelegramPassport;

import java.util.UUID;

public class LoginWithTelegram implements SocialNetworkLogin {

    private static LoginWithTelegram INSTANCE;
    private Context context;
    private String payload=UUID.randomUUID().toString();
    private TelegramListener mTelegramListener;

    private LoginWithTelegram(Context context, TelegramListener telegramListener){
        this.context = context;
    }

    public static LoginWithTelegram getInstance(Context context, TelegramListener telegramListener){
        if (INSTANCE == null){
            INSTANCE = new LoginWithTelegram(context, telegramListener);
        }
        return INSTANCE;
    }

    @Override
    public void login() {
        TelegramPassport.AuthRequest params=new TelegramPassport.AuthRequest();
        params.botID=546349;
        params.nonce=payload;
        params.publicKey = "-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAruw2yP/BCcsJliRoW5eB\n" +
                "VBVle9dtjJw+OYED160Wybum9SXtBBLXriwt4rROd9csv0t0OHCaTmRqBcQ0J8fx\n" +
                "hN6/cpR1GWgOZRUAiQxoMnlt0R93LCX/j1dnVa/gVbCjdSxpbrfY2g2L4frzjJvd\n" +
                "l84Kd9ORYjDEAyFnEA7dD556OptgLQQ2e2iVNq8NZLYTzLp5YpOdO1doK+ttrltg\n" +
                "gTCy5SrKeLoCPPbOgGsdxJxyz5KKcZnSLj16yE5HvJQn0CNpRdENvRUXe6tBP78O\n" +
                "39oJ8BTHp9oIjd6XWXAsp2CvK45Ol8wFXGF710w9lwCGNbmNxNYhtIkdqfsEcwR5\n" +
                "JwIDAQAB\n" +
                "-----END PUBLIC KEY-----";

        params.scope=new PassportScope(
                PassportScope.PHONE_NUMBER
        );
        TelegramPassport.request((LoginActivity) this.context, params, MainViewModel.TELEGRAM_SIGN_IN);
    }

    @Override
    public void activityResult(int requestCode, int resultCode, Intent data) {

    }
}
