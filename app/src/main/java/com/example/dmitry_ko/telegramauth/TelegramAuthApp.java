package com.example.dmitry_ko.telegramauth;

import android.app.Application;

import com.example.dmitry_ko.telegramauth.data.SharedPrefUtils;

public class TelegramAuthApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        SharedPrefUtils mData = SharedPrefUtils.getINSTANCE();
        mData.setContext(this);
    }
}
