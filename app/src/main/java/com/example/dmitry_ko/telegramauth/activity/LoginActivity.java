package com.example.dmitry_ko.telegramauth.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dmitry_ko.telegramauth.R;
import com.example.dmitry_ko.telegramauth.databinding.ActivityLoginBinding;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.MainViewModel;

public class LoginActivity extends AppCompatActivity {

    private MainViewModel mMainViewModel;
    private ActivityLoginBinding mLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    private void initViewModel(){
        mLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mMainViewModel = new MainViewModel(this);
        mLoginBinding.setMViewModel(mMainViewModel);
    }

    @Override
    protected void onDestroy() {
        mMainViewModel.unsubscribe();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mMainViewModel.resultActivity(requestCode, resultCode, data);
    }

    private void goToTheSecondActivity(String verifiedCode){
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("CODE_SMS_VERIFIED", verifiedCode);
        this.startActivity(intent);
    }
}
