package com.example.dmitry_ko.telegramauth.api;

import com.example.dmitry_ko.telegramauth.model.TelegramApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private final String BASE_URL_TELEGRAM = "https://core.telegram.org/api/";
    private final String BASE_URL_VIBER = "https://api.viber.org/";

    private static RetrofitClient INSTANCE;

    private Gson gson;
    private Retrofit mRetrofit;

    private RetrofitClient(){}

    public static RetrofitClient getINSTANCE() {
        if (INSTANCE == null){
            INSTANCE = new RetrofitClient();
        }
        return INSTANCE;
    }

    private Gson getGson(){
        if (gson == null){
            gson = new GsonBuilder().setLenient().create();
        }
        return gson;
    }

    private Retrofit getRetrofit(String BaseUrl){
        if (mRetrofit == null){
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(BaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return mRetrofit;
    }

    public TelegramApi getTelegramApi(){
        return getRetrofit(BASE_URL_TELEGRAM).create(TelegramApi.class);
    }
}
