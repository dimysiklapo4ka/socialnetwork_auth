package com.example.dmitry_ko.telegramauth.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;

import com.example.dmitry_ko.telegramauth.BR;

public class SecondViewModel extends BaseObservable {

    private String mResponseText;

    @Bindable
    public void setText(String mResponseText){
        Log.e(SecondViewModel.class.getSimpleName(), mResponseText);
        this.mResponseText = mResponseText;
        notifyPropertyChanged(BR.text);

    }

    public String getText(){
        return mResponseText;
    }
}
