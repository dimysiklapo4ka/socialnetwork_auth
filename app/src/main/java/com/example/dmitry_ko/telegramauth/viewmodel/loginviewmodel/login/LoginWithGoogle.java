package com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login;

import android.content.Context;
import android.content.Intent;

import com.example.dmitry_ko.telegramauth.activity.LoginActivity;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.SocialNetworkLogin;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.login.base.listener.GoogleListener;
import com.example.dmitry_ko.telegramauth.viewmodel.loginviewmodel.MainViewModel;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;

public class LoginWithGoogle implements SocialNetworkLogin {
    private static LoginWithGoogle INSTANCE;
    private Context context;
    private GoogleApiClient mGoogleApiClient;
    private GoogleListener mGoogleListener;

    private LoginWithGoogle(Context context, GoogleListener googleListener){
        this.context = context;
        mGoogleListener = googleListener;
        initGoogleAuth();
    }

    public static LoginWithGoogle getInstance(Context context, GoogleListener googleListener){
        if (INSTANCE == null){
            INSTANCE = new LoginWithGoogle(context, googleListener);
        }
        return INSTANCE;
    }

    private void initGoogleAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void login() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((LoginActivity)context).startActivityForResult(signInIntent, MainViewModel.GOOGLE_SIGN_IN);
    }

    @Override
    public void activityResult(int requestCode, int resultCode, Intent data) {
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        if (task.isSuccessful()) {
            mGoogleListener.googleResult(task);
        }
    }
}
